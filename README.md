# MOGU

mesh of GUN user

This is a project started in SHLUG following https://disaster.radio. Our aim is to build a free mesh in Shanghai area using easy-to-access hardware and free software.

The development will follow [disaster-radio](https://github.com/sudomesh/disaster-radio) based on the idea of [disaster radio](https://disaster.radio/). Its mailinglist is: https://sudoroom.org/lists/listinfo/disasterradio

- Hardware: LILYGO® TTGO Disaster-Radio LoRa32 V2.1 1.6 Version 433/868/915MHZ LoRa ESP-32 OLED 0.96 Inch SD Card Bluetooth WIFI Module
we use 915MHz. Please check [LILYGO](https://www.aliexpress.com/item/4000396836096.html)
CONFIGURATION: https://docs.platformio.org/page/boards/espressif32/ttgo-lora32-v1.html
PLATFORM: Espressif 32 1.12.4 #08ef4bf > TTGO LoRa32-OLED V1
HARDWARE: ESP32 240MHz, 320KB RAM, 4MB Flash

- Battery: LILYGO T-Higrow ESP32WiFi/BT/DHT11 battery - 3.7V Lithium battery

## Setup

- Hardware requirement

1. computer with GNU/Linux system
2. USB cable (Type-A male + micro-B male)


- Software requirement

1. minicom: It is used for serail communication, e.g., debugging.

`sudo aptitude install minicom`

2. python, python-pip, platformio, pio: These are for firmware building.

`sudo aptitude install python python-pip`

3. git: We use it to download required packages.

`sudo aptitude install git`

4. PACKAGES:
 - framework-arduinoespressif32 3.10004.200129 (1.0.4)
 - tool-esptoolpy 1.20600.0 (2.6.0)
 - toolchain-xtensa32 2.50200.80 (5.2.0)

```
sudo aptitude install esptool
pip install --upgrade esptool
```

5. nvm: We use it to install node, npm: We need it to build web app.

[git install nvm: node version manager](https://github.com/nvm-sh/nvm)


## Firmware installation

- initial setup

Please edit platformio.ini to make sure your use the correct board
(e.g., ttgo-lora32-v2) and `upload_port` is set to the correct device
(e.g., /dev/ttyUSB0).


```
cd firmware
sudo pip install -U platformio
```

Note: in Guix, this is installed in ~/.local/bin, which is not in PATH.


- building firmware

```
cd firmware
~/.local/bin/pio upgrade --dev
~/.local/bin/pio update
~/.local/bin/pio run
```

The firmware will be put under

`.pio/build/ttgo-lora32-v2/firmware.bin`

- Flashing firmware

Connect the computer to the board using a usb cable,

In `platformio.ini` make sure `upload_port`, `upload_speed`, etc are correct for the board.
Port setting is `11520 8-N-1 No hardware`.

Then run:

```
cd firmware
~/.local/bin/pio run -t upload
```

By default, PlatformIO builds and flashes firmware for the LILY's ESP32 TTGO V2 dev board. If you would like to build for another supported board, select the corresponding build environment. For example to build and flash the firmware and file system for the ESP32 TTG0 V1 board, use the following,

`~/.local/bin/pio run -e ttgo-lora32-v1 -t upload -t uploadfs`

- test

Debugging can be done over serial using a tty interface, such as screen
or minicom. To test the web app:

Using a PC or phone and connect to the SSID provided by the device,
`disaster.radio <MAC address>`,
navigate to
`http://disaster.local or http://192.168.4.1`
enter a nick
send a message
to toggle the local echo, type $

## web app build

- initial setup

```
nvm install 7.10.1 # we need use this node version
nvm install-latest-npm # optional
nvm use 7.10.1
cd web
npm install
cp settings.js.example settings.js
npm run build-css
npm run build # build the js and css
npm start # run the simulator server
```

When these are successful, you can open the following in your browser:

```
[http://localhost:8000/](http://localhost:8000/)
```

It has the exact interface as `http://disaster.local or http://192.168.4.1`.

- Building and uploading SPIFFS image

To only build:

```
cd firmware
~/.local/bin/pio run -t buildfs
```

To build and upload:

`~/.local/bin/pio run -t uploadfs`

The SPIFFS is in directory: `firmware/.pio/build/ttgo-lora32-v2/spiffs.bin`.

If using an SD card, copy the contents of `web/static/` to the root of a fat32 formatted SD card, then insert the SD card.

## Testing the system

Once the firmware and SPIFFS image has been successfully flashed, you can test it by logging into the disaster.radio <node-address> wireless network and navigating to http://192.168.4.1 in a browser to access the demo chat app.

You can also try connecting to the device via serial or telnet with commands like,

```
cd firmware
~/.local/bin/pio device monitor
```

or

`telnet 192.168.4.1`

After pressing enter once, you should be greeted with a banner and presented with a prompt to enter a nick or send an anonymous message.

## Creating Binary for Release

A full binary image can be created by reading the flash contents of a ESP32 that is flashed with the latest release of the firmware. To do this, run the following command,

`~/.local/bin/esptool.py -p /dev/ttyUSB0 -b 921600 read_flash 0 0x400000 esp_flash.bin`

This can then be flashed to a new device like so,

`~/.local/bin/esptool.py -p /dev/ttyUSB0 --baud 460800 write_flash 0x00000 esp_flash.bin`

## Development

If you're including new libraries in the firmware, for PlatformIO, you wil need to add them to `platformio.ini` under `lib_deps`.

If you would like to make changes to a specific library, such as `LoRaLayer2`, you can clone the library into the `firmware/lib` folder that is created after running `pio run` and then comment it out or remove it from the `lib_deps` list in the `platformio.ini` file.

For developing web application, use

```
cd web
npm run dev # starts simulator server and auto-builder
```

The disaster.radio firmware opens up a websocket using the [ESPAsyncWebServer](https://github.com/me-no-dev/ESPAsyncWebServer) library. Through this, client-side javascript can transmit and receive messages over the LoRa tranceiver. If you'd like to build an application for disaster.radio, you could write a websocket client that sends and receives messages in the same format. Currently, the firmware expects websocket messages in the following format,

```
<msgID><msgType>|<msg>
```

where,

```
<msgID> is a two-byte binary unsigned integer representing an abitrary sequence number, this is sent back to the websocket client with an ! appended to act as an acknowledgment and could be used for error-checking,

<msgType> is a single binary utf8 encoded character representing the application for which the message is intended, such 'c' for chat, 'm' for maps, or 'e' for events

<msg> is a binary utf8 encoded string of characters limited to 236 bytes, this can be treated as the body of the message and may be used to handle client-side concerns, such as intended recipient or requested map tile.
```

An example messge may appear as follows,

```
0100c|<noffle>@juul did you feel that earthquake!
```

Alternatively, you could write another Layer3 client for as part of the disater radio firmware and create you own Layer 4 message format. See more about our networkng stack on our wiki, [Layered Model](https://github.com/sudomesh/disaster-radio/wiki/Layered-Model).